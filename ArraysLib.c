/**
 * Copyright (C) 2020 Fabio Sussarellu
 *
 * This file is part of Array.h.
 *
 * Array.h is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Array.h is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Array.h.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ArraysLib.h"
#include "Array.c"

/**
 * Return a new ArraysLib instance
 */
ArraysLib newArraysLib() {
    ArraysLib l;

    l.allocate        = Array__allocate;
    l.clone           = Array__clone;
    l.copy            = Array__copy;
    l.countValidItems = Array__countValidItems;
    l.free            = Array__free;
    l.getMinOrMax     = Array__getMinOrMax;
    l.move            = Array__move;
    l.new             = Array__new;
    l.print           = Array__print;
    l.resize          = Array__resize;
    l.rotate          = Array__rotate;
    l.minimize        = Array__minimize;
    l.get             = Array__get;
    l.set             = Array__set;
    l.add             = Array__add;
    l.delete          = Array__delete;
    l.swap            = Array__swap;

    return l;
}