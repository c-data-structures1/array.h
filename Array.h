/**
 * Copyright (C) 2020 Fabio Sussarellu
 *
 * This file is part of Array.h.
 *
 * Array.h is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Array.h is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Array.h.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARRAY_H
#define ARRAY_H

// * DNT
#include "CUtils.h/CUtils.h"

/**
 * Generic Array
 */
typedef struct Array *Array;
struct Array {
    Item *Items;
    unsigned int ItemsNumber;
    // Function for delecting a single object
    void (*freeItem)(Item);
    // Function for printing a object
    void (*printItem)(Item);
};
// * EDNT

#endif // ! ARRAY_H
