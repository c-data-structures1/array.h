# Copyright (C) 2020 Fabio Sussarellu
# 
# This file is part of sistemi-operativi.
# 
# sistemi-operativi is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# sistemi-operativi is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with sistemi-operativi.  If not, see <http://www.gnu.org/licenses/>.

Array=Array
ArrayLibrary=ArraysLib
build_folder=builds
compiler=tcc

$(build_folder)/$(ArrayLibrary).a: $(build_folder)/$(ArrayLibrary).o $(build_folder)/$(Array).o CUtils.h/builds/CUtils.o
	ar rvs $(build_folder)/$(ArrayLibrary).a $(build_folder)/$(Array).o CUtils.h/builds/CUtils.o $(build_folder)/$(ArrayLibrary).o

$(build_folder)/$(ArrayLibrary).o: $(build_folder)
	$(compiler) -c -o $(build_folder)/$(ArrayLibrary).o $(ArrayLibrary).c

$(build_folder)/$(Array).o: $(build_folder)
	$(compiler) -c $(Array).c -o $(build_folder)/$(Array).o

CUtils.h/builds/CUtils.o:
	make -C CUtils.h

$(build_folder):
	mkdir -p $(build_folder)

clean:
	rm -rf $(build_folder)

test: $(build_folder)/$(ArrayLibrary).a
	$(compiler) -o $(build_folder)/test test.c $(build_folder)/$(ArrayLibrary).a