/**
 * Copyright (C) 2020 Fabio Sussarellu
 *
 * This file is part of Array.h.
 *
 * Array.h is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Array.h is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Array.h.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ArraysLib.h"

ArraysLib Arrays;

typedef int *Int;
Int newInt(int value) {
    Int i = malloc(sizeof(*i));
    *i    = value;
    return i;
}

int main(int argc, char const *argv[]) {
    Arrays = newArraysLib();

    Array a = (Array)Arrays.new(NULL, NULL);
    Arrays.allocate(a, 10);

    for (uint i = 0; i < a->ItemsNumber; i++) {
        a->Items[i] = newInt(i);
    }
    Arrays.print(a);

    printf("Trying to add extra values, right now array size it's: %d\n\n",
           a->ItemsNumber);
    uint originalSize = a->ItemsNumber;
    for (uint i = originalSize; i < originalSize + 5; i++) {
        Arrays.add(a, newInt(i));
    }
    Arrays.minimize(a, false); // Reduce the size of the array
    Arrays.print(a);
    printf("Array size it's %d items\n", a->ItemsNumber);
    Arrays.free(a, true);

    return 0;
}
