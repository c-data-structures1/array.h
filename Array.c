/**
 * Copyright (C) 2020 Fabio Sussarellu
 *
 * This file is part of Array.h.
 *
 * Array.h is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Array.h is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Array.h.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Array.h"

// * Fallback functions

/**
 * Fallback function for printObject, by default works with int
 */
static void printFallback(Item i) {
    if (i == NULL) {
        printf("NULL ");
        return;
    }
    printf("%d ", *(int *)i);
}

/**
 * Return true if array's index exist
 */
static bool validIndex(Array a, uint index) { return a->ItemsNumber < index; }

/**
 * Resize an array, delete exceeding items
 * If freeObjects it's true delete also items inside the array
 */
void Array__resize(Array a, unsigned int newSize, bool freeItems) {
    Item *new = calloc(newSize, sizeof(Item));

    unsigned int minSize = a->ItemsNumber < newSize ? a->ItemsNumber : newSize;
    for (unsigned int i = 0; i < minSize; i++) { // For every copyable item
        new[i] = a->Items[i];
    }

    if (a->ItemsNumber > newSize) { // If sice it's inferior
        if (freeItems) {
            for (unsigned int i = minSize; i < a->ItemsNumber;
                 i++) { // For every exceeding item
                a->freeItem(a->Items[i]);
            }
        }
    } else if (a->ItemsNumber < newSize) {
        for (unsigned int i = minSize; i < newSize;
             i++) { // For every exceeding item
            new[i] = NULL;
        }
    }

    free(a->Items);           // Delete array
    a->Items       = new;     // Replace array
    a->ItemsNumber = newSize; // Update size
}

/**
 * Get item at index from array, if index doesn't exist return NULL
 */
Item Array__get(Array a, uint index) {
    if (validIndex(a, index)) { return NULL; }
    return (Item)a->Items[index];
}

/**
 * Double an Array's size
 */
static void doubleArraySize(Array a) {
    Array__resize(a, a->ItemsNumber * 2, false);
}

/**
 * Count the number of items inside the array excluding the NULL items in the
 * end
 */
static uint realArraySize(Array a) {
    uint i = 0;
    for (i = a->ItemsNumber - 1; Array__get(a, i) == NULL && (int)i != -1; i--)
        ; // Count all the NULL values in the end of the array
    return i + 1;
}

void Array__minimize(Array a, bool freeItems) {
    Array__resize(a, realArraySize(a), freeItems);
}

// * End of Fallback functions
/**
 * Allocate array
 */
void Array__allocate(Array a, unsigned int ObjectsNumber) {
    if (a->Items != NULL) { // If it's already allocated
        return Array__resize(a, ObjectsNumber, true);
    }

    a->ItemsNumber = ObjectsNumber;
    a->Items       = (Item *)calloc(a->ItemsNumber, sizeof(Item));
    for (unsigned int i = 0; i < a->ItemsNumber;
         i++) { // Write NULL in all the array
        a->Items[i] = NULL;
    }
}

/**
 * Copy SRC in DEST, copy all the minium number of elements between the two
 */
void Array__copy(Array DEST, Array SRC) {
    DEST->freeItem = SRC->freeItem;

    unsigned int min =
        DEST->ItemsNumber < SRC->ItemsNumber
            ? DEST->ItemsNumber
            : SRC->ItemsNumber; // Calcolo il numero minore di elementi

    for (unsigned int i = 0; i < min; i++) {
        DEST->Items[i] = SRC->Items[i];
    }
}

/**
 * Return the number of valid items inside the array acording to a validation
 * function
 */
unsigned int Array__countValidItems(Array a, bool (*valid)(Item i, Item args),
                                    Item args) {
    unsigned int items = 0;
    for (unsigned int i = 0; i < a->ItemsNumber; i++) {
        if ((*valid)(a->Items[i], args)) { items++; }
    }
    return items;
}

/**
 * Create an array
 * freeObject = Function for delecting a single object
 * printObject = Function for parsing a object from string
 */
Array Array__new(void (*freeObject)(Item), void (*printObject)(Item)) {
    Array a        = (Array)malloc(sizeof(struct Array));
    a->Items       = NULL;
    a->ItemsNumber = 0;
    a->freeItem    = freeObject == NULL ? &free : freeObject;
    a->printItem   = printObject == NULL ? &printFallback : printObject;
    return a;
}

/**
 * Create and allocate a new array with a copy of every single item of SRC
 * return the array
 */
Array Array__clone(Array SRC) {
    Array DEST = Array__new(NULL, NULL);
    Array__allocate(DEST, SRC->ItemsNumber);
    Array__copy(DEST, SRC);
    return DEST;
}

/**
 * Create a new array with only the valid items of a according to a
 * validation function, if none it's valid return NULL
 */
Array Array__validItems(Array a, bool (*valid)(Item i, Item args), Item args) {
    unsigned int items = Array__countValidItems(a, valid, args);
    if (items == 0) { return NULL; }

    Array valids = Array__new(a->freeItem, a->printItem);
    Array__allocate(valids, items);

    unsigned int validsIndex = 0;
    for (unsigned int i = 0; i < a->ItemsNumber; i++) {
        if ((*valid)(a->Items[i], args)) {
            valids->Items[validsIndex++] = a->Items[i];
        }
    }

    return valids;
}

/**
 * Return max/min in the array according to a compare function
 * compare(item a, item b) = return true if a come first in order than b
 */
Item Array__getMinOrMax(Array a, bool (*compare)(Item a, Item b)) {
    Item extreme = a->Items[0];
    for (unsigned int i = 0; i < a->ItemsNumber; i++) {
        extreme = (*compare)(a->Items[i], extreme) ? a->Items[i] : extreme;
    }
    return extreme;
}

/**
 * Swap position for item i inside the array with the one in the index position
 */
void Array__move(Array a, Item i, unsigned int position) {
    unsigned int positionI = (unsigned int)-1;
    for (unsigned int j = 0; j < a->ItemsNumber; j++) {
        if (a->Items[j] == i) { // If they are equals
            positionI = j;
            break;
        }
    }

    if (positionI == (unsigned int)-1) { // Interrupt if index isn't found
        return;
    }

    // Swap items
    Item tmp            = a->Items[position];
    a->Items[position]  = a->Items[positionI];
    a->Items[positionI] = tmp;
}

/**
 * Delete an array
 * If freeObjects it's true delete also items inside the array
 */
void Array__free(Array a, bool freeObjects) {
    if (a == NULL) { return; }
    freeObjects = freeObjects && a->freeItem != NULL ? true : false;

    if (a->Items != NULL) {
        if (freeObjects) {
            for (unsigned int i = 0; i < a->ItemsNumber; i++) {
                if (a->Items[i] != NULL) { a->freeItem(a->Items[i]); }
            }
        }
        free(a->Items);
    }
    free(a);
}

/**
 * Print an array
 */
void Array__print(Array a) {
    if (a == NULL) { return; }

    for (unsigned int i = 0; i < a->ItemsNumber; i++) {
        if (a->Items[i] != NULL) { a->printItem(a->Items[i]); }
    }
    printf("\n");
}

/**
 * Return a new array with the same items as "a" and the range defined by begin
 * and end, return NULL in case of errors
 */
Array Array__split(Array a, uint begin, uint end) {
    // Check ranges
    if (begin >= a->ItemsNumber || end <= begin) { return NULL; }

    // Create the new array
    uint newSize = end - begin;
    Array new    = Array__new(a->freeItem, a->printItem);
    Array__allocate(new, newSize);

    // Copy all the data
    uint newIndex = 0;
    for (uint i = begin; i < a->ItemsNumber; i++) {
        new->Items[newIndex++] = a->Items[i];
    }

    return new;
}

/**
 * Rotate array of "rotation" positions
 */
void Array__rotate(Array a, int rotation) {
    bool sign = false;

    // Check sign
    if (rotation < 0) {
        sign = true;
        rotation *= -1;
    }

    // Adjust rotation value
    rotation %= a->ItemsNumber;
    if (rotation == 0) { return; }
    if (sign) { rotation = a->ItemsNumber - rotation; }

    Item *tmpArray = calloc(a->ItemsNumber, sizeof(Item));

    unsigned int index = 0;
    for (unsigned int i = rotation; i < a->ItemsNumber;
         i++) { // Copy the first elements
        tmpArray[i] = a->Items[index++];
    }
    for (unsigned int i = 0; i < rotation; i++) { // Copy the remaining ones
        tmpArray[i] = a->Items[index++];
    }

    free(a->Items);
    a->Items = tmpArray;
}

/**
 * Swap two items inside an array, referred by index (use at your own risk)
 */
void Array__swap(Array a, uint i, uint j) {
    Item tmpI = i >= a->ItemsNumber ? NULL : a->Items[i];
    Item tmpJ = j >= a->ItemsNumber ? NULL : a->Items[j];
    if (i < a->ItemsNumber) { a->Items[i] = tmpJ; }
    if (j < a->ItemsNumber) { a->Items[j] = tmpI; }
}

/**
 * Set the value of an Index item
 */
void Array__set(Array a, uint index, Item i) {
    if (validIndex(a, index)) { return; }
    a->Items[index] = i;
}

/**
 * Add an item to the end of the Array
 */
void Array__add(Array a, Item i) {
    uint itemsNumber = realArraySize(a);
    if (itemsNumber == a->ItemsNumber) { doubleArraySize(a); }
    Array__set(a, itemsNumber, i);
}

/**
 * Free item in index and set NULL
 */
void Array__delete(Array a, uint index) {
    Item tmp = Array__get(a, index);
    if (tmp == NULL) { return; }
    a->freeItem(tmp);
    Array__set(a, index, NULL);
}