/**
 * Copyright (C) 2020 Fabio Sussarellu
 *
 * This file is part of Array.h.
 *
 * Array.h is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Array.h is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Array.h.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARRAYSLIB_H
#define ARRAYSLIB_H

// * DNT
#include "Array.h"

typedef struct ArraysLib {
    void (*swap)(Array a, uint i, uint j);
    void (*rotate)(Array a, int rotation);
    Array (*split)(Array a, uint begin, uint end);
    void (*print)(Array a);
    void (*free)(Array a, bool freeObjects);
    void (*move)(Array a, Item i, unsigned int position);
    Item (*getMinOrMax)(Array a, bool (*compare)(Item a, Item b));
    Array (*validItems)(Array a, bool (*valid)(Item i, Item args), Item args);
    Array (*new)(void (*freeObject)(Item), void (*printObject)(Item));
    Array (*clone)(Array SRC);
    unsigned int (*countValidItems)(Array a, bool (*valid)(Item i, Item args),
                                    Item args);
    void (*copy)(Array DEST, Array SRC);
    void (*resize)(Array a, unsigned int newSize, bool freeItems);
    void (*allocate)(Array a, unsigned int ObjectsNumber);
    void (*add)(Array a, Item i);
    void (*set)(Array a, uint index, Item i);
    void (*minimize)(Array a, bool freeItems);
    Item (*get)(Array a, uint index);
    void (*delete)(Array a, uint index);
} ArraysLib;

// * EDNT

ArraysLib newArraysLib();

#endif // ! ARRAYSLIB_H
